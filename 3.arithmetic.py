x = 100.3422222
y = 10

# Common arithmetic operators
print(f"x + y  = {x + y}")
print(f"x - y  = {x - y}")
print(f"x * y  = {x * y}")
print(f"x / y  = {x / y}")
print(f"x // y = {x // y}")  # x – (x // y) * y
print(f"x ** y = {x ** y}")
print(f"x % y  = {x % y}")
print(f"-x     = {-x}")
print(f"+x     = {+x}")

# # Common mathematic operators
print(f"abs({x}) = {abs(x)}")
print(f"divmod({x}, {y}) = {divmod(x, y)}")  # x // y, x % y
print(f"power({x}, {y}) = {pow(x, y)}")  # x ** y
print(f"round({x}, 1) = {round(x)}")
