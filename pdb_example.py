def get_path():
    filename = __file__

    print(f'path = {filename}')
    print("This is line 5 and Some more lines")
    print("This is line 6 and Some 6lines")

# Option 1
# import pdb; pdb.set_trace()


# Option 2
# import pdb
# pdb.set_trace()

# option 3
# __import__('pdb').set_trace() # Same as import pdb; pdb.set_trace()

# option 4 
breakpoint()

get_path()
