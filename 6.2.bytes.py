# a = 4
# list1 = [1, 2, 3, 4, 5]

# # No argument case
# print("Byte conversion with no arguments : " + str(bytes()))

# # conversion to bytes
# print("The integer conversion results in : " + str(bytes(a)))
# print("The iterable conversion results in : " + str(bytes(list1)))

str1 = "CafÖr"
print(
    "Byte conversion with ignore error : " + str(bytes(str1, "ascii", errors="ignore"))
)

# Giving utf-8 encoding and replace error
print(
    "Byte conversion with replace error : "
    + str(bytes(str1, "ascii", errors="replace"))
)

# Giving ascii encoding and strict error
# throws exception
# print ("Byte conversion with strict error : " +
#       str(bytes(str1, 'ascii', errors = 'strict')))
