import paramiko
import argparse



def main(hostname, username, password, port):
    # Create object of SSHClient and
    # connecting to SSH
    ssh = paramiko.SSHClient()
    ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    # set_missing_host_key_policy(AutoAddPolicy)
    ssh.connect(hostname, port=port, username=username, password=password, timeout=3,  allow_agent=False)


    # Execute command on SSH terminal
    # using exec_command
    stdin, stdout, stderr = ssh.exec_command('uptime')
    lines = stdout.readlines()
    print(lines)

parser = argparse.ArgumentParser(prog='My SSH Client')

# Hostname is required parameter
parser.add_argument('--hostname', dest='hostname', help='Hostname to connect', required=True)
# Optional parameters
parser.add_argument('-u', '--username', dest='username', help='Username to connect to the host', default='vagrant')
parser.add_argument('-p', '--password', dest='password', help='Password to connect to the host', default='vagrant')

parser.add_argument('--port', dest='port', help='Port number to connect the host', type=int, default=22)

args = parser.parse_args()

hostname = args.hostname
password = args.password
username = args.username
port = args.port

main(hostname=hostname, username=username, password=password, port=port)
