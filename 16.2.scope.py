# def a():
#     print(s)

# s = 10 # global
# a()


# import pdb
# def b():
#     # pdb.set_trace()
#     var1asdas = 11 # Local variable for this function
#     print(var1)

# var1 = 12 # global variable for the b function
# b()


s = 10
# def c():
#     global s
#     print(s)

# c()

def d():
    global s
    print(s)
    # s = 13
    var1 = 12
    print(var1)


d()
print(s)
print(var1) # this will raise error