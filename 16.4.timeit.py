import timeit

statements_1 = """
def odd(x):
    ret = []
    for i in range(0, x + 1):
        if i % 2 != 0:
            ret.append(i)
    return ret
odd(1000001)
"""

statements_2 = """
a = [i for i in range(0, 1000001) if i % 2 != 0]
"""

# Calculates UDF time
print(timeit.timeit(stmt=statements_1, number=100))
# Calculates list comprehension time
print(timeit.timeit(stmt=statements_2, number=100))

