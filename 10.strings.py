single_quote = "Hello World"
double_quote = "Hello World"
triple_quote = """Hello 
This is another line
World"""

print(single_quote, double_quote, triple_quote)

print("a" "b" "c")

a = "Hello World"

print(len(a))
print(a[4])
print(a[-1])
print(a[:4])
print(a[:-1])
print(a[-5:])
