import paramiko

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

host = "localhost"
user = "vagrant"
passwd = "vagrant"
port = 2222

ssh.connect(hostname=host, username=user, password=passwd, port=port)

stdin, stdout, stderr = ssh.exec_command('date')
output = []
for line in stdout.readlines():
    output.append(line)
print(output)

ssh.close()