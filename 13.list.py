names = ["Earth", "Mars", "Jupiter"]
# ->        0,        1,    2
# <-       -3,       -2,   -1
print(len(names))
# print(names[1])
# print(names[-2])
# print(names[0:])

# names.append("Uranus")
# print(names)
# print(len(names))
# names.insert(0, 'Mercury')
# print(names)


# a = ['x', 'y', 'y']
# b = ['z', 'z']

# print(a + b)

# letters = list()
# letters = list("Dave")
# print(letters)

# a = [1, 'Dave', 3.14, ['Mark', 7, 9, [100, 101]], 10]
# print(a)
