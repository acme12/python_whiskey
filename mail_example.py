import smtplib

# From Address
fromaddr = "root@localhost"
# To Address
toaddr = "vagrant@localhost"
# Message
msg = ("From: %s\r\nTo: %s\r\n\r\nHello" % (fromaddr, toaddr))

# Creating SMTP instance using localhost
server = smtplib.SMTP('localhost')
# Setting debuglevel
server.set_debuglevel(1)
# Send actual message using from, to
server.sendmail(fromaddr, toaddr, msg)
# Exit the SMTP session
server.quit()
