pdb
====

* import pdb; pdb.set_trace()
* __import__('pdb').set_trace()
* breakpoint()
* q
* p
* pp
* s (step)
* n (next)
* ll (longlist)
* l (list)
* b (breakpoint)
* b <LINE_NUMBER>
* b <FUNCTION_NAME>
* b - list breakpoints
* disable <BREAKPOINT_NUMBER>
* enable <BREAKPOINT_NUMBER>
* cl <BREAKPOINT_NUMBER> (clear)
