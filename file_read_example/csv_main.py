import csv
import sys
import os
import wmi


# Check user input
if len(sys.argv) == 1:
    # User has not provided any input file, so set it to default
    filename = "./server_list.txt"
    print("User has not provided any filename using %s" % filename, file=sys.stderr)
else:
    # User wants specific file to be read, use it
    filename = sys.argv[1]

if not os.path.exists(filename):
    print("Failed to find the filename: %s" % filename)
    sys.exit(2)

with open(filename) as fh:
    # Option 1
    # reader = csv.reader(fh)
    # next(reader)
    # for row in reader:
    #     print(row)

    # Option 2
    reader = csv.DictReader(fh)
    # print(reader)
    for row in reader:
        # print(row, type(row))
        if row['hostname'].startswith('#'):
            continue
        
        if row['password'] is None:
            continue

        # row => {'hostname': 'localhost', 'username': 'username1', 'password': 'passw0rd'}
        hostname = row['hostname']
        # hostname = row.get('hostname')
        username = row['username']
        # username = row.get('username')
        password = row['password']
        # password = row.get('password')

        # print("I have data of server %s with username %s and password %s" % (hostname, username, password))
        if hostname == 'localhost':
            # WMI crys for combination of localhost with username and password so skip it
            conn = wmi.WMI()
        else:
            # WMI requires username and password for remote machines so use gathered information
            conn = wmi.WMI(hostname, user=username, password=password)
        
        # Print headers
        print("=" * 80)
        # Gather the disk information from the respective server and print it for user and continue
        print("Getting disk information for server %s" % hostname)
        for disk in conn.Win32_LogicalDisk():
            if disk.size != None:
                print(disk.Caption, "is {0:.2f}% free".format(100*float(disk.FreeSpace)/float(disk.Size)))
        # Print footer
        print("=" * 80)