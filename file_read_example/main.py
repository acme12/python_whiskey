# Open file
# open("./server_list.txt")
import sys
import os
import wmi

# Check user input
if len(sys.argv) == 1:
    # User has not provided any input file, so set it to default
    filename = "./server_list.txt"
    print("User has not provided any filename using %s" % filename, file=sys.stderr)
else:
    # User wants specific file to be read, use it
    filename = sys.argv[1]

# Option 1
# try:
#     fh = open(filename)
# except FileNotFoundError:
#     print("Failed to find the filename %s" % filename)
#     sys.exit(2)

# Option 2
if not os.path.exists(filename):
    print("Failed to find the filename: %s" % filename)
    sys.exit(2)
fh = open(filename)

# Option 2 ends here, if you want to comment the comment till here

# print(fh.read())

# for line in fh.read():
first_line = 0

# print(fh.readlines())
for line in fh.readlines():
    # Skipping the first header line
    if first_line == 0:
        first_line = first_line + 1
        continue
    # Skipping the lines which starts with hash or comment
    if line.startswith('#'):
        continue
    # Keep the line count for any logic that follows
    first_line = first_line + 1
    
    # Convert line to upper since "line" is a string
    #print("This is a line : %s" % line.upper())

    # Each line has three parts so split using common (,)
    line_data = line.split(",")

    # print("Password with newline: %s " % line_data[2])
    # print("Password without newline: %s" % line_data[2].strip())

    # Check if data is in correct format i.e. 3 variables in one line otherwise skip it
    if len(line_data) != 3:
        print("Corrupt data found on line %s. Skipping" % first_line, file=sys.stderr)
        continue

    # Store the variable in appro. variable name for further processing
    hostname = line_data[0]
    username = line_data[1]
    # line ends with new line character so remove addition carriage return from it.
    password = line_data[2].strip()
    
    # print("I have data of server %s with username %s and password %s" % (hostname, username, password))
    if hostname == 'localhost':
        # WMI crys for combination of localhost with username and password so skip it
        conn = wmi.WMI()
    else:
        # WMI requires username and password for remote machines so use gathered information
        conn = wmi.WMI(hostname, user=username, password=password)
    
    # Print headers
    print("=" * 80)
    # Gather the disk information from the respective server and print it for user and continue
    print("Getting disk information for server %s" % hostname)
    for disk in conn.Win32_LogicalDisk():
        if disk.size != None:
            print(disk.Caption, "is {0:.2f}% free".format(100*float(disk.FreeSpace)/float(disk.Size)))
    # Print footer
    print("=" * 80)

fh.close()