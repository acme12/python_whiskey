for i in range(0, 10):
    print(i)

print("Done")

# break
for i in range(0, 10):
    if i == 5:
        break
    print(i)

print("Done")
# continue
for i in range(0, 10):
    if i == 5:
        continue
    print(i)
print("Done")


for i in range(0, 14, 3):
    print(i)


a = ["a", "b", "c", "d", "e"]
for idx, value in enumerate(a):
    print(f"{a[idx]} at {idx}")
