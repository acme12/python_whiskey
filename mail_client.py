# Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.message import EmailMessage

textfile = "./mail_message.txt"

# Normal method to open file and read content and close the file afterwards
# fp = open(textfile)
# content = fp.read()
# fp.close()


# Open the plain text file whose name is in textfile for reading.
with open(textfile) as fp:
    # Create a text/plain message
    msg = EmailMessage()
    msg.set_content(fp.read())

# me == the sender's email address
# you == the recipient's email address
me = "root@localhost"
you = "vagrant@localhost"

# msg['Subject'] = f'The contents of {textfile}'
msg['Subject'] = 'The contents of %s' % textfile
msg['From'] = me
msg['To'] = you

# Send the message via our own SMTP server.
s = smtplib.SMTP('localhost')
s.send_message(msg)
s.quit()