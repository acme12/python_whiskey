s = {"name": "GOOG", "shares": 100, "price": 490.10}

print(s["name"])
print(s.get("aaa", False))

prices = {"GOOG": 490.1, "AAPL": 123.5, "IBM": 91.5, "MSFT": 52.13}

print(prices.keys())
print(prices["GOOG"])

del prices["GOOG"]
print(prices)

prices = dict()
prices[("IBM", "2015-02-03")] = 91.23
prices["IBM", "2015-02-04"] = 91.42
print(prices)
