#!/usr/bin/env python3

# Talks, Walks, Behave may be is duck

principal: int = 1000  # Initial amount
rate: float = 0.05  # Interest rate
numyears: int = 15  # Number of years
year: int = 1

while year <= numyears:
    principal = principal * (1 + rate)  # expression
    # Normal Print
    # print(year, principal)
    # print("%s %s" % (year, principal))
    print("{0} {1}".format(year, principal))

    # f-string
    # print(f'{year:>3d} {principal:0.2f}')
    # print(f'{year:>3d} {principal:0.3f}')
    # print(f'year: {year} Amount of Principal: {principal}')

    year += 1
