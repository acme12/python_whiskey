a = 10

print("a = %s" % a)
a += 1
print("a + 1 = %s" % a)

a -= 1
print("a - 1 = %s" % a)

a *= 10
print("a * 10 = %s" % a)

a /= 10
print("a / 10 = %s" % a)

a //= 2
print("a // 2 = %s" % a)

a **= 5
print("a ** 5 = %s" % a)
