class DeviceError(Exception):
    def __init__(self, errno=0, msg="Default Error"):
        self.args = (errno, msg)
        self.errno = errno
        self.errmsg = msg


# Raises an exception (multiple arguments)
try:
    # raise DeviceError(1, 'Not Responding')

    # raise DeviceError(2, 'Head is not spinning')

    raise DeviceError()
except DeviceError as e:
    print("Program exited with %s and gave error message %s " % (e.errno, e.errmsg))
