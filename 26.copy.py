a = [1, 2, 3, 4]
print(a)
b = a  # b is reference of a
print(b)
print(b is a)
b[2] = -100
print(b)
print(a)

a = [1, 2, [3, 4]]
b = list(a)  # create shallow copy of a
print(a)
print(b)
print(b is a)
b.append(100)
print(b)
print(a)
b[2][0] = 100
print(b)
print(a)

import copy

a = [1, 2, [3, 4]]
print(a)
b = copy.deepcopy(a)
print(b)
b[2][0] = 100
print(b)
print(a)
