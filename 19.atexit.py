import atexit


def open_connection():
    """
    Open database and do some important stuff
    """
    print("Hello")


def cleanup_connection():
    """
    Clean up method.
    1. Close all database connection
    2. Remove all temporary files from /tmp
    3. Close all file handles
    """
    print("Bye")


open_connection()
atexit.register(cleanup_connection)
