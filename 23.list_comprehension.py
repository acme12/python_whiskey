# List comprehension
nums = range(0, 6)
squares = [n * n for n in nums if n > 2]  # [9, 16, 25]
print(squares)

# set comprehension
portfolio = [
    {"name": "IBM", "shares": 100, "price": 91.1},
    {"name": "MSFT", "shares": 50, "price": 45.67},
    {"name": "HPE", "shares": 75, "price": 34.51},
    {"name": "CAT", "shares": 60, "price": 67.89},
    {"name": "IBM", "shares": 200, "price": 95.25},
]

names = {s["name"] for s in portfolio}
print(names)

# Dict comprehension

prices = {s["name"]: s["price"] for s in portfolio}
print(prices)
