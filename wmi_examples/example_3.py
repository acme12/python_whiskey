# start target service
import wmi
conn = wmi.WMI()

result = 1

state_dict = {
    0: "The request was accepted.",
    1: "The request is not supported.",
    6: "The service has not been started.",
}

for s in conn.Win32_Service(StartMode="Auto", State="Stopped"):
    if 'MapsBroker' == s.Name:
        # import pdb; pdb.set_trace()
        result, = s.StartService()
        print("The startservice returned %s" % result)
        # if result == 0:
        #     print("Successfully started service:", s.Name)
        # if result == 1:
        #     print("The request is not supported.", s.Name)
        current_state, = s.InterrogateService()
        print("Current state of %s : %s %s" % (s.Name, current_state, state_dict.get(current_state)))