import wmi

conn = wmi.WMI()

# for process in conn.Win32_Process():
#     print("ID: {0} HandleCount: {1} ProcessName: {2}".format(process.ProcessId, process.HandleCount, process.Name))

# filtering specific processes
for process in conn.Win32_Process(name="firefox.exe"):
    if process.HandleCount > 1000: # only processes with handle count above 1000
        print(process.ProcessID, process.HandleCount, process.Name)


pid, returnval= conn.Win32_Process.Create(CommandLine="notepad.exe")

# kill the process using process id
# conn.Win32_Process(ProcessId=pid)[0].Terminate()