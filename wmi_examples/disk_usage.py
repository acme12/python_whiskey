import wmi


conn = wmi.WMI("xxx", user=r"xxx", password="xxx")


for disk in conn.Win32_LogicalDisk():
 if disk.size != None:
   print(disk.Caption, "is {0:.2f}% free".format(
   100*float(disk.FreeSpace)/float(disk.Size))
 )
