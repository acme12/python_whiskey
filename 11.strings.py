x = "42"
y = "67"

print(x + y)

s = "HELLO"
print(s.endswith("O"))
print(s.startswith("H"))
print(s.find("EL"))  # index is found, -1 if not found
print(s.lower())
print(s.lower().upper())
print(s.title())
print(s.replace("E", "e"))
print(s.strip("O"))
print(s.strip("H"))


s = "hello\nworld"
print(str(s))
print(repr(s))
