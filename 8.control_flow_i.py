# Wallrus operator
x = 1
while (x := x + 1) < 10:  # Prints 1, 2, 3, ..., 9
    print(x)

print("Done")

# Break
x = 0
while x < 10:
    if x == 5:
        break  # Stops the loop. Moves to Done below
    print(x)
    x += 1

print("Done")

# continue
x = 0
while x < 10:
    x += 1
    if x == 5:
        continue  # Skips the print(x). Goes back to loop start.
    print(x)

print("Done")
