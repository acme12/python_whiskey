# import sys
# import traceback


a = []
try:
    a = 10 / 0
except EOFError:
    print("File ended")
except IndentationError:
    print("Something is wrong with Indentation")
except (MemoryError, ZeroDivisionError):
    print("Divided by Zeroo")
except ImportError:
    print("Exception")
    # traceback.print_exc(file=sys.stdout)
