# if

a = 20
b = 20

# if a < b:
#     print("a is less than b")
# else:
#     print("a is greater than b")


# elif
# Check the extension/suffix and set content type based upon that
# suffix = ".pn"
# if suffix == '.htm':
#     content = 'text/html'
# elif suffix == '.jpg':
#     content = 'image/jpeg'
# elif suffix == '.png':
#     content = 'image/png'
# elif suffix == '.pn':
#     content = "Unknown file format"
# else:
#     content = None

# print(f"{content}")


# one liner
maxval = b if a > b else b
print(maxval)
