* Binary Types: memoryview, bytearray, bytes
* Boolean Type: bool
* Set Types: frozenset, set
* Mapping Type: dict
* Sequence Types: range, tuple, list
* Numeric Types: complex, float, int
* Text Type: str