import argparse

# Create a parser
parser = argparse.ArgumentParser(prog='Calculator', usage='%(prog)s [options]')
# usage: Calculator [options]


# Operation
parser.add_argument('--operation', dest='operation', choices=['add', 'subtract', 'divide', 'multiply'], help='Operation that user need to perform')

# Number1
parser.add_argument('--number_one', dest='number_one', help='First number for the operation')

# Number2
parser.add_argument('--number_two', dest='number_two', help='Second number for the operation')


args = parser.parse_args()
# print(args.operation)
# print(args.number_one)
# print(args.number_two)


if args.operation == 'add':
    addition = int(args.number_one) + int(args.number_two)
    print("Addition of %s and %s is %s" % (args.number_one, args.number_two, addition))
elif args.operation == 'subtract':
    subtract = int(args.number_one) - int(args.number_two)
    print("Subtraction of %s and %s is %s" % (args.number_one, args.number_two, subtract))
elif args.operation == 'multiple':
    multiple = int(args.number_one) * int(args.number_two)
    print("Multiplication of %s and %s is %s" % (args.number_one, args.number_two, multiple))
elif args.operation == 'divide':
    divide = int(args.number_one) / int(args.number_two)
    print("Division of %s and %s is %s" % (args.number_one, args.number_two, divide))
else:
    print("Operation not supported. Only (add, subtract, divide, multiple)")
