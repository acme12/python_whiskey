def remainder(a, b):
    """
    Computes the remainder of dividing a by b
    """
    q = a // b
    r = a - q * b
    return r


r = remainder(11, 2)
print(r)


def divide(a, b):
    q = a // b  # If a and b are integers, q is integer
    r = a - q * b
    return (q, r)


d = divide(11, 2)
print(d)
