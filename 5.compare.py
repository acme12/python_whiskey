a = 10
b = 2

print(f"{a} == {b} => {a==b}")
print(f"{a} <  {b} => {a<b}")
print(f"{a} >  {b} => {a>b}")
print(f"{a} >= {b} => {a>=b}")
print(f"{a} <= {b} => {a<=b}")
