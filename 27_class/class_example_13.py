class A:
    def __init__(self):
        self.__x = 3  # Mangled to self._A__x

    def __spam(self):  # Mangled to _A__spam()
        print("A.__spam", self.__x)

    def bar(self):
        self.__spam()  # Only calls A.__spam()


class B(A):
    def __init__(self):
        A.__init__(self)
        self.__x = 37  # Mangled to self._B__x

    def __spam(self):  # Mangled to _B__spam()
        print("B.__spam", self.__x)

    def grok(self):
        self.__spam()  # Calls B.__spam()”


b = B()
# instead of b.__spam()
b.grok()
print(vars(b))
a = A()
a.bar()
print(vars(a))

# b.bar()
# b.grok()
# print(vars(b))
