class Base(object):
    def __init__(self):
        print("Base init'ed")

    def print_me(self):
        print("I am from Base Class")

class ChildA(Base):
    def __init__(self):
        print("ChildA init'ed")
        Base.__init__(self)

    # def print_me(self):
    #     print("I am from ChildA")

class ChildB(Base):
    def __init__(self):
        print("ChildB init'ed")
        Base.__init__(self)

    # def print_me(self):
    #     print("I am from ChildB")


# class ChildC(ChildA, ChildB):
class ChildC(ChildA, ChildB):
    def __init__(self):
        print("ChildC init'ed")
        # ChildA.__init__(self)
        # ChildB.__init__(self)
        import pdb; pdb.set_trace()
        super(ChildC, self).__init__()
        super(ChildB, self).__init__()

    def print_mro(self):
        mro = type(self).mro()
        print(mro)

    # def print_me(self):
    #     print("I am from ChildC")

class ChildD(ChildC):
    def __init__(self):
        ChildC.__init__(self)

    def print_mro(self):
        mro = type(self).mro()
        print(mro)

# c_a = ChildA()
# c_b = ChildB()
# b = Base()
c_c = ChildC()
c_c.print_me()
# c_d = ChildD()
# c_d.print_mro()
# c_d.print_me()

"""
            Base
              |
            /   \
         childA ChildB
            \   /
              |
            ChildC
              |
            childD
              |
            /    \
          childE ChilDF
"""