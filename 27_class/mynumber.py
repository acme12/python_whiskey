class Complex:
    def __init__(self, real, img):
        self.real = real
        self.img = img

    def __repr__(self):
        return "(%s + %si)" % (self.real, self.img)

    def __add__(self, other):
        answer_real = self.real + other.real
        answer_img = self.img + other.img
        return Complex(answer_real, answer_img)

    def __mul__(self, other):
        answer_real = self.real * other.real
        answer_img = self.img * other.img
        return Complex(answer_real, answer_img)

c_one = Complex(1, 2)
c_two = Complex(3, 4)

print("This is my first complex number %s" % c_one)
print("This is my second complex number %s " % c_two)

print("The Addition above two complex number %s" % (c_one + c_two))

print("The multiplication of two complex number %s " % (c_one * c_two))