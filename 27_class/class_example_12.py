class Account:
    def __init__(self, owner, balance):
        self.owner = owner
        self.__balance = balance

    def __repr__(self):
        return f"Account({self.owner!r}, {self._balance!r})"

    def deposit(self, amount):
        print("I am depositing %s " % amount)
        self.__balance += amount

    def withdraw(self, amount):
        print("I am withdrawing %s" % amount)
        self.__balance -= amount

    def inquiry(self):
        print("I am inquiring about my balance")
        return self.__balance

    def __print_me(self):
        print("This is private to Account class only and will not inherited to EvilAccount")

class EvilAccount(Account):
    def __init__(self, owner, balance):
        Account.__init__(self, owner, balance)

    def nasty_withdrawal(self, amount):
        self.__balance = self.__balance - amount


ev_acc = EvilAccount("Tej", 1000)
print(ev_acc.inquiry())
# ev_acc.nasty_withdrawal(10)
# ev_acc.nasty_withdrawal(10)
# ev_acc.nasty_withdrawal(10)
# print(ev_acc.inquiry())
ev_acc.__print_me()
# a = Account("A", 1000)
# print(a.inquiry())
# a.deposit(100)
# print(a.inquiry())
# a.deposit(20)
# print(a.inquiry())
# a.withdraw(120)
# print(a.inquiry())