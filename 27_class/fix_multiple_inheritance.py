class First():
    def __init__(self):
        super(First, self).__init__()
        print('First')

    def print_me(self):
        print("I am First")

class Second():
    def __init__(self):
        super(Second, self).__init__()
        print('Second')

    def print_me(self):
        print("I am Second")

class Fourth():
    def __init__(self):
        super(Fourth, self).__init__()
        print('Fourth')

    def print_me(self):
        print("I am fourth")

class Third(First, Second, Fourth):
    def __init__(self):
        super(Third, self).__init__()
        print('Third')

t = Third()
t.print_me()


