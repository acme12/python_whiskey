class Account:
    def __init__(self, owner, balance):
        self.owner = owner
        self._balance = balance

    @property
    def owner(self):
        print("Property is called")
        return self._owner

    @owner.setter
    def owner(self, value):
        print("Set property is called")
        if not isinstance(value, str):
            raise TypeError("Expected str")
        self._owner = value


a = Account("David", 1000)

print(a.owner)

try:
    b = Account(1, 1)
except Exception as e:
    print("Exception raised %s" % e)
