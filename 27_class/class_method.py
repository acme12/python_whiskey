

class Cake():
    def __init__(self, flour):
        self.flour = flour

    @classmethod
    def bake(cls, flour):
        print("Do something related to baking %s" % flour)

class Pizza():
    def __init__(self, flour):
        self.flour = flour
 
    @classmethod
    def bake(cls, flour):
        print("Do something related to baking %s" % flour)

