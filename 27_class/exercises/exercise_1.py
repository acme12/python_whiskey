
class Vehicle(object):
    """
    Create a Vehicle class with max_speed and mileage
    instance attributes
    """
    def __init__(self, max_speed, mileage, name, color):
        self.max_speed = max_speed
        self.mileage = mileage
        self.name = name
        self.color = color


v1 = Vehicle(180, 50, "Volvo", "White")
print("This is my car which has max speed %s and mileage %s and name is %s and color %s" % (v1.max_speed, v1.mileage, v1.name, v1.color)) # getter

