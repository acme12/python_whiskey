import sys

class Tesla:
    def __init__(self):
        print("tesla called")

class Dog:
    pass

#print(__name__) # __main__

#print(sys.modules.keys())
#import pdb
#pdb.set_trace()

import inspect

clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)

for class_name, class_obj in clsmembers:
    if class_name == 'Tesla':
        class_obj()
        break

