"""
Create a Vehicle class without any variables and methods
"""
class Vehicle(object):
    NON_LIVING = True
    def __init__(self, weight, price=1):
        self.weight = weight
        self.price = price
        self.color = 'RED'
        self.country = 'US'
        if self.country == 'US':
            self.unit = ' lbs'
            self.currency = 'DOLLAR'
        elif self.country == 'BR':
            self.unit = ' BRS'
            self.currency = ' BRA DOLLAR'
        else:
            self.unit = ' kgs'
            self.currency = ' INR'

    def get_weight(self):
        return self.weight + self.unit

    def get_price(self):
        return str(self.price + self.price * 10 / 100) + self.currency


# ----8<----Cut---here-----8<------

# v1 = Vehicle(10, 10000)
# v1 = Vehicle(weight=10, price=10000)
# v1 = Vehicle(10)
# v1 = Vehicle(weight=10)
# v1 = Vehicle(10, 1000)
v1 = Vehicle(weight='10', price=1000)
print(v1.weight)
print("The vehicle weight %s " % v1.get_weight())
print("The price of the vehicle %s " % v1.get_price())

# b = v1.get_weight()
# print(b)
# print(v1.NON_LIVING)
# print("The weight of v1 :%s" % v1.weight)


# Car Class
class Car(Vehicle):
    pass

# c1 = Car()
# print(c1.NON_LIVING)


# WhiteCar Class
class WhiteCar(Car):
    COLOR = 'White'
    NON_LIVING = False


# wc1 = WhiteCar()
# print(wc1.NON_LIVING, wc1.COLOR)

# Truck Class
class Truck(Vehicle):
    pass

# Monster Truck
class MonsterTruck(Truck):
    pass

# Airplane Class
class Airplane(Vehicle):
    pass

# Fighter Jet
class FighterJet(Airplane):
    pass