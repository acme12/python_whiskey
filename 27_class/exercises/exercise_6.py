class Car:
    """
    Car Base Class
    """
    def __init__(self, country='US'):
        # Set the country
        self.country = country
        # Set the base price for Car
        self.base_price = 100
        # Decide on currency and commission depending upon
        if self.country == 'US':
            self.commission = 10
            self.currency_unit = 'USD'
        elif self.country == 'UK':
            self.commission = 12
            self.currency_unit = 'GBP'
        else:
            raise ValueError("Please specify a valid country code. Possible values are 'US', 'UK'. You provided '%s'." % country)

    def calculate_commission(self):
        """
        Calculates the commission related to the car depending
        upon the country and the commission rate
        """
        final_value = self.base_price + self.base_price * self.commission / 100
        # return self.currency_unit + str(final_value)
        return self.currency_unit, final_value

class Tesla(Car):
    """
    Tesla Car Class
    """
    def __init__(self, country):
        # Call the init of the base class so that we can access the respecitive methods and attributes
        Car.__init__(self, country=country)
        # Override the base price of Tesla
        self.base_price = 1000

class BMW(Car):
    """
    BMW Car Class
    """
    def __init__(self, country):
        # Call the init of the base class so that we can access the respecitive methods and attributes
        # Just another way of calling init. See Tesla Class for other example.
        super().__init__(country=country)
        # Override the base price of BMW
        self.base_price = 2000

# # Define country code first
# country_code = 'US'
# # Either can be called
# # Create which car you want
# model_s = Tesla(country=country_code)
# # model_s = Tesla('US')
# curreny_unit, final_price = model_s.calculate_commission()
# print("Telsa costs %s %s in %s" % (curreny_unit, final_price, country_code))

# # Again define the country code
# country_code = 'UK'
# bmw_x2 = BMW(country=country_code)
# curreny_unit, final_price = bmw_x2.calculate_commission()
# print("BMW costs %s %s in %s" % (curreny_unit, final_price, country_code))

# # Let us fail this
# country_code = 'IN'
# try:
#     bmw_x2 = BMW(country_code)
# except ValueError as e:
#     print("Error in country code: %s" % e)


tries = 3
country_code_flag = False
while tries > 0:
    tries = tries - 1
    country_code = input("Enter Country code (Valid values are 'US', 'UK'): ")
    if country_code not in ('US', 'UK'):
        print("You entered wrong choice: '%s'. Valid values are 'US', 'UK'." % country_code)
        continue
    else:
        country_code_flag = True
        break

if country_code_flag:
    print(country_code)
else:
    print("You exhausted 3 tries to input country code")

model = input("Enter name of the car you want (Valid values are 'BMW', 'Tesla'): ")

# Option 1 
# class_name = eval(model)  # Converts string into class name
# car = class_name(country_code)

# Option 2
# import sys
# car = getattr(sys.modules[__name__], model)
# print(type(car))

# Option 3
import sys
import inspect

clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)
required_class = None
for class_name, class_obj in clsmembers:
    if class_name == model:
        required_class = class_obj
        break

car = required_class(country=country_code)

curreny_unit, final_price = car.calculate_commission()
print("%s costs %s %s in %s" % (model, curreny_unit, final_price, country_code))
