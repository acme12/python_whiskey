# def print_me(name, age, sex, balance, height, weight, state,):
#     print(name, age, sex, balance, height, weight, state)

# print_me('J', 12, 'M', 1000, 170, 60, 'MH')

# def print_me(*var):
#     for arg in var:
#         print(arg, end=",")

# print_me('J', 12, 'M', 1000, 170, 60, 'MH', 'IN')
# print_me('Hello', 'Welcome', 'to', 'Python3')

# def print_me_two(arg1, arg2, *argv):
#     print("Arg1 = %s" % arg1)
#     print("Arg2 = %s" % arg2)
#     for arg in argv:
#         print(arg, end=" ")

# print_me_two('Hello', 'Welcome', 'to', 'Python3')
# print_me_two('Jay', 'Shinde')


# def print_me_three(**kwargs):
#     for key, value in kwargs.items():
#         print("%s => %s" % (key, value))
#         if key == 'balance':
#             print("My balance is %s Dollars" % value)

# print_me_three(first='Hello', second='Welcome', third='to', fourth='Python3')

# print_me_three(age=12, name='Jay', state='MH', country='IN', balance=1000)

def print_me_four(*args, **kwargs):
    for arg in args:
        print(arg)
    for k, v in kwargs.items():
        print("%s => %s" % (k, v))

print_me_four(1, 2, 3, first='Hello', second='Welcome', third='to', fourth='Python3')
