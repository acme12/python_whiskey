class Ops:
    """
    Example of staticmethod
    """

    @staticmethod
    def add(x, y):
        return x + y

    @staticmethod
    def multiple(x, y, z):
        return x * y * z

    def divide(self, x, y):
        return x / y

o = Ops()
print(o.add(1, 2))
print("The multiplication of 3, 4, 5 is %s" % o.multiple(3, 4, 5))
print("The division of 10 and 5 is %s " % o.divide(10, 5))