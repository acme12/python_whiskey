# Interfaces


class Stream:
    def send(self, msg):
        raise NotImplementedError()

    def receive(self):
        raise NotImplementedError()


def send_request(stream: Stream, request):  # type-hints aren’t enforced
    if not isinstance(stream, Stream):
        raise TypeError("Expected Stream")
    stream.send(request)
    return stream.receive()


class SocketStream(Stream):
    def send(self, msg):
        print("Send from SocketStream : %s" % msg)

    def receive(self):
        return "Receive from SocketStream"


s = SocketStream()
print(send_request(s, "Hello World"))
print(send_request("a", "a"))
