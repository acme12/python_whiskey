class Cat:
    """
    Simple Dog Class
    """

    name: str
    age: int

    species = "Felis silvestris"

    def __init__(self, name, age, color):
        self.name = name
        self.age = age
        self.color = color

    def __str__(self):
        return "<Cat %s>" % self.name

    def __repr__(self):
        return "Cat('%s', '%s')" % (self.name, self.age)

    def describe(self):
        print(
            "I am %s of type '%s' and I am %s years old"
            % (self.name, self.species, self.age)
        )

    def speak(self):
        print("%s says Meow" % self.name)


c = Cat("Felix", 4, 'Brown')
# c.describe()
# c.speak()
# print(str(c))
# print(repr(c))

# c2 = Cat("Eva", 1)
# # c2.describe()
# # c2.speak()
# print(str(c2))
# print(repr(c2))

# vars
# print(vars(c))


# # hasattr
# print(hasattr(c, "name"))
# # getattr
# print(getattr(c, "name", None))

# # delattr
# delattr(c, "name")
# print(vars(c))

# # setattr
# setattr(c, "name", "Delix")
# print(vars(c))
