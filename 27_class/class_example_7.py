# Overriding
class Animal:
    def message(self):
        print("Print from Animal")

    def jump(self):
        print("I am jumping")


class Dog(Animal):
    def message(self):
        print("Print from Dog")
        # Animal.message(self)
        Animal.jump(self)

    def jump_and_print_message(self):
        Animal.message(self)
        # Animal.jump(self)
        self.jump()

d = Dog()
# d.message()
# d.jump()
d.jump_and_print_message()
