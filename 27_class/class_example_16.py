# Abstract Class
from abc import ABC, abstractmethod


class Stream(ABC):
    @abstractmethod
    def receive(self):
        pass

    @abstractmethod
    def send(self, msg):
        pass


# s = Stream()  # Won't work


def send_request(stream: Stream, request):  # type-hints aren’t enforced
    if not isinstance(stream, Stream):
        raise TypeError("Expected Stream")
    stream.send(request)
    return stream.receive()


class SocketStream(Stream):
    def send(self, msg):
        print("Send from SocketStream : %s" % msg)

    def receive(self):
        return "Receive from SocketStream"


s = SocketStream()
print(send_request(s, "Hello World"))
