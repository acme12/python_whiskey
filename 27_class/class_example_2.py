class Dog:
    """
    Simple Dog Class
    """

    name: str
    age: int

    species = "Canis familiaris"

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def describe(self):
        print(
            "I am %s of type %s and I am %s years old"
            % (self.name, self.species, self.age)
        )

    def speak(self):
        print("%s says Woof" % self.name)


if __name__ == "__main__":
    d = Dog("CheeseCake", 5)
    d.describe()
    d.speak()
    print(d.species)