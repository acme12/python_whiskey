# class Account:
#     """
#     A class method is a method that is applied to the class itself, not to instances.
#     """

#     def __init__(self, owner, balance):
#         self.owner = owner
#         self.balance = balance

#     @classmethod
#     def from_data(cls, data):
#         return cls(data[0], data[1])


# a1 = Account("A", 1)

# a2 = Account.from_data(("B", 2))

# print(a1.owner)
# print(a2.owner)

# a3 = Account.from_data(("Tej", 1000))
# print("My name is %s and my account balance is %s " % (a3.owner, a3.balance))


class Square:
    # def __init__(self, length, breadth):
    #     self.length = length
    #     self.breadth = breadth

    @classmethod
    def calculate_area(cls, length, breadth):
        return length * breadth


class Rectangle:
    pass
    # def __init__(self, length, breadth):
    #     self.length = length
    #     self.breadth = breadth

    # def calculate_area(self, length, breadth):
    #     return length * breadth

class Triangle:
    pass

# s1 = Square()
# r1 = Rectangle()

print("The Area of square is %s " % Square.calculate_area(4, 4))
print("The Area of rectangle is %s" % Square.calculate_area(5, 10))
print("The Area of Triangle is %s" % Square.calculate_area(3, 3))

