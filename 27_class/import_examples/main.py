# import pkg.ex2


# To Use Animal class here

import ex1


# a = ex1.Animal()

dog1 = ex1.Dog()

cat1 = ex1.Cat()

animal1 = ex1.Animal()


from ex1 import Animal, Dog, Cat

dog2 = Dog()

cat2 = Cat()

animal2 = Animal()


# from ex1 import *  # THIS IS NOT RECOMMENDED as it will pollute your namespace


from ex1 import Animal as ex1_animal
# from ex2 import Animal as ex2_animal

# from somebigmoudlename import somebigvariablenamewithnounderscoreorsomethinglikethat as n

animal1 = ex1_animal()


