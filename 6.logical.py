# print("=" * 80)
# # AND
# for i_a in [False, True]:
#     for i_b in [False, True]:
#         print(f"{i_a} and {i_b} = {i_a and i_b}")


# print("=" * 80)
# # OR
# for i_a in [False, True]:
#     for i_b in [False, True]:
#         print(f"{i_a} or {i_b} = {i_a or i_b}")


# print("=" * 80)
# # NOT
# for i_a in [False, True]:
#     print(f"not {i_a} = {not i_a}")

# print("=" * 80)
