# Context manager


class Manager:
    def __init__(self, x):
        self.x = x

    def yow(self):
        print("Main function is called")

    def __enter__(self):
        print("enter is called")
        return self

    def __exit__(self, ty, val, tb):
        print("exit is called")


with Manager(42) as m:
    m.yow()
