a = 0b00111100  # 60
b = 0b00001101  # 13

print(f"{a:>08b} & {b:>08b} = {a & b:>08b}")  # 12  => 00001100
print(f"{a:>08b} | {b:>08b} = {a | b:>08b}")  # 61  => 00111101
print(f"{a:>08b} ^ {b:>08b} = {a ^ b:>08b}")  # 49  => 00110001
print(f"~{a}     = {~a}")  # -61 => 11000011
print(f"{a:>08b} << 2  = {a << 2:>08b}")  # 240 => 11110000
print(f"{a:>08b} >> 2  = {a >> 2:>08b}")  #  15 => 00001111
