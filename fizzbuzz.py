
def fizzbuzz(n):
    n = int(n)
    for i in range(1, n + 1):
        if i % 3 == 0 and i % 5 == 0:
            print("FizzBuzz")
        # if the number is divisible by 3
        elif i % 3 == 0:
            print("Fizz")
        # if the number is divisible by 5
        elif i % 5 == 0:
            print("Buzz")
        else:
            print(i)

# user provides some number, we have to iterate
n = input("Enter a number to print FizzBuzz pattern: ")
fizzbuzz(n)
