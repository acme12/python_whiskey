def print_upper(text):
    return text.upper()

def print_lower(text):
    return text.lower()

def print_extra_line(text):
    print("=" * 80)
    print("This is output")
    print("=" * 80)
    return text

def info(text):
    print("INFO")
    return text

def debug(text):
    print("DEBUG")
    return text

def greet(func):
    greeting = func("Good Morning")
    print(greeting)

def happy_birthday(function_name):
    greet_hbd = function_name("Happy Birthday to You")
    print(greet_hbd)

def say_sorry(function_name):
    print(function_name('I am really sorry'))

# greet(print_lower)
# greet(print_upper)


# happy_birthday(print_upper)
# happy_birthday(print_lower)

# say_sorry(print_extra_line)
say_sorry(info)
say_sorry(debug)

# def make_pretty(function_name):
#     def inner():
#         print("I got decorated")
#         function_name()
#     return inner


# def ordinary():
#     print("I am ordinary")

# ordinary()
# pretty = make_pretty(ordinary)
# pretty()