a = {"IBM", "MSFT", "AA"}

print(a)

b = set(["IBM", "MSFT", "HPE", "IBM", "CAT"])
print(b)

c = b
c.add("DIS")
print(c)

print(a & c)  # intersection
print(a | c)  # Union
print(a - c)  # Difference
print(c - a)  # Difference


c.update({"JJ", "GE", "ACME"})
print(c)
c.remove("JJ")
print(c)
c.discard("GE")
print(c)
