from enum import Enum

class Role(Enum):
    PRESIDENT = 1
    VICEPRESIDENT = 2
    MANAGER = 3
    CONTRIBUTOR = 4
    INTERN = 5


class Employee:
    def __init__(self, emp_type):
        self.emp_type = emp_type

# emp_type = input("Enter employee integer type: ")
# print(emp_type)
e1 = Employee(Role.MANAGER)
print(e1.emp_type)

if e1.emp_type == Role.PRESIDENT:
    print("Pay me as per president")
elif e1.emp_type == Role.MANAGER:
    print("Pay me as manager and give me bonus")
else:
    print("Other type of employee")

# if e1.emp_type == 'President':
#     print("E1 is president")
# else:
#     print("E1 is not president")

# e2 = Employee(Role.PRESIDENT)

# if e2.emp_type == Role.PRESIDENT:
#     print("e2 is president")
# else:
#     print("e2 is not president")

# # e1.emp_type == e2.emp_type